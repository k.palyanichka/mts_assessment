from dataclasses import dataclass
import time



@dataclass
class TestTube:
    is_positive: bool
    number: int


class Mouse:
    def __init__(self, number: int):
        self.number: int = number
        self.status: str = 'live'  # [live , dead] - возможные статусы мыши
        self.tube_n: int = None # № пробирки от которой умерла мышь

    def eat(self, tt):
        if tt.is_positive is True:
            self.tube_n = tt.number
            self.status = 'dead'


def wait():
    # print("Ждем результата проверки ")
    # time.sleep(1)
    pass

def day_of_death(n):
    if n <= 3:
        return 2
    elif n > 3 and n<=6:
        return 3
    else:
        return 4

def check_mouses_status(mouses):
    '''
    Проверка статусы мыши. В случае если мышь умерла, то возращаем номер пробирки от которой она скончалась
    '''
    for mouse in mouses:
        if mouse.status == 'dead':
            print(f"Мышь №{mouse.number} умерла от пробирки №{mouse.tube_n} на {day_of_death(mouse.tube_n)} день")
            return True, mouse.tube_n


def ifelse(current, target):
    '''
    доп ф-ция для определения отравленной пробирки
    '''
    if current == target:
        return True
    else:
        return False




def variation_1(target_tube):
    '''
    Вариант "комбинаторика"
    '''
    test_tubes = [TestTube(ifelse(s, target_tube), s) for s in range(1, 9)]
    mouses_per_days = [Mouse(1), Mouse(2), Mouse(3)] * 3
    waited = []

    for tube, mouse in zip(test_tubes, mouses_per_days):
        #     print(tube.is_positive, tube.number)
        mouse.eat(tube)
        waited.append(mouse)
        if (tube.number % 3 == 0 and tube.number != 1) or tube.number == 8:
            wait()
            res = check_mouses_status(waited)
            if res is not None and res[0]:
                #             print(tube.number)
                return res[1]
            else:
                waited = []

def variation_2(target_tube):
    '''
    вариант "бинарный поиск"
    '''
    # оперделяем тестовые пробирки и указываем какая пробирка отравленная
    test_tubes = [TestTube(ifelse(s, target_tube), s) for s in range(1, 9)]
    def _reshape(lst, n):
        '''
        конвертация 1d массив в 2d массив
        '''
        return [lst[i:i + n] for i in range(0, len(lst), n)]

    reshaped = _reshape(test_tubes, 3)
    combined_tubes = []

    for idx, arr in enumerate(reshaped):
        # смешиваем пробирки
        combined_tubes.append(TestTube(bool(sum([s.is_positive for s in arr])), idx + 1))

    mouses = [Mouse(1), Mouse(2), Mouse(3)]
    waited = []

    # поиск отравленного микса
    for tube, mouse in zip(combined_tubes, mouses):
        mouse.eat(tube)
        waited.append(mouse)

    target_tube = check_mouses_status(waited)
    # оставляем только живых мышей
    live_mouses = list(filter(lambda x: x.status != 'dead', mouses))

    waited = []
    #берем пробирки с которого был сделан отравленный микс
    new_tubes = reshaped[target_tube[1] - 1]

    #ищем отравленную пробирку
    for tube, mouse in zip(new_tubes, live_mouses):
        mouse.eat(tube)
        waited.append(mouse)

    res = check_mouses_status(waited)
    # обе мыши остались живыми и нам нужна пробирка которую они не попробовали
    if res is None:
        return new_tubes[-1].number
    else:
        return res[1]

if __name__ == '__main__':
    poisoned_tube = int(input("Введите какая пробирка отравлена:"))
    variation_1(poisoned_tube)
    variation_2(poisoned_tube)