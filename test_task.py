import unittest
from task import variation_1, variation_2

class MyTestCase(unittest.TestCase):
    def test_variation_1(self):
        poisoned_tube  = 5
        res = variation_1(poisoned_tube)
        self.assertEqual( poisoned_tube, res)

    def test_variation_1(self):
        poisoned_tube = 5
        res = variation_2(poisoned_tube)
        self.assertEqual(poisoned_tube, res)



if __name__ == '__main__':
    unittest.main()
